FROM alpine:3.12

RUN set -x && apk add --no-cache \
     acf-dovecot dovecot-mysql dovecot-pop3d dovecot-lmtpd dovecot-submissiond\
     dovecot-pigeonhole-plugin rspamd-client ca-certificates netcat-openbsd bash

# Remove the existing folders and create empty folders
RUN rm -rf /etc/dovecot
RUN rm -rf /var/lib/dovecot && mkdir /var/lib/dovecot

COPY config /etc/dovecot

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Unencrypted and encrypted pop
EXPOSE 110/tcp 995/tcp

# Uncrypted and encrypted imap
EXPOSE 143/tcp 993/tcp

# Email Submission port and LMTP
EXPOSE 587/tcp 1025/tcp

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/usr/sbin/dovecot", "-c", "/etc/dovecot/dovecot.conf", "-F"]
